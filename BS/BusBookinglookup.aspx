﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageSignIn.master" AutoEventWireup="true" CodeFile="BusBookinglookup.aspx.cs" Inherits="BS_BusBookinglookup" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link href="<%=ResolveUrl("~/css/lytebox.css")%>" rel="stylesheet" type="text/css" />

    <link type="text/css" href="<%=ResolveUrl("~/Styles/jquery-ui-1.8.8.custom.css") %>" rel="stylesheet" />
    <%--<script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-1.4.4.min.js") %>"></script>--%>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/gridview-readonly-script.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-ui-1.8.8.custom.min.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/AgencySearch.js") %>"></script>

   

    <div class="row">
          <div class="container-fluid" style="padding-right: 285px">
            <div class="page-wrapperss">

                <div class="panel panel-primary">
             
                    <div class="panel-body">

                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">From Date</label>
                                    <input type="text" name="From" id="From" class="input-text full-width" readonly="readonly" />
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">To Date</label>
                                    <input type="text" name="To" id="To" class="input-text full-width"" readonly="readonly" />
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">OrderId</label>
                                    <asp:TextBox ID="txtOrderID" runat="server" CssClass="input-text full-width" MaxLength="50"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">PNR</label>
                                    <asp:TextBox ID="txtPnr" runat="server" CssClass="input-text full-width" MaxLength="50"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="row">

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Source</label>
                                    <asp:TextBox ID="TxtSource" runat="server" CssClass="input-text full-width" MaxLength="100"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Destination</label>
                                    <asp:TextBox ID="TxtDestination" runat="server" CssClass="input-text full-width" MaxLength="100"></asp:TextBox>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Ticket No</label>
                                    <asp:TextBox ID="txtTicketNo" runat="server" CssClass="input-text full-width" MaxLength="50"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">BusOperator</label>
                                    <asp:TextBox ID="txtBusOperator" runat="server" CssClass="input-text full-width" MaxLength="150"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <%-- <div class="col-md-3">
                                <div class="form-group" runat="server" cssclass="form-control">
                                    <label for="exampleInputPassword1">Status</label>
                                    <asp:DropDownList ID="ddl_Status" runat="server" CssClass="form-control">
                                        <asp:ListItem Value="BOOKED">Booked</asp:ListItem>
                                        <asp:ListItem Value="Cancelled">Cancelled</asp:ListItem>
                                        <asp:ListItem Value="Hold">Hold</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>--%>

                            <div class="col-md-4" id="td_Agency" runat="server">
                                <div class="form-group" id="td_Agency1" runat="server">
                                    <label for="exampleInputPassword1">Agency Name</label>
                                    <input type="text" id="txtAgencyName" class="input-text full-width" name="txtAgencyName" onfocus="focusObj(this);"
                                        onblur="blurObj(this);" defvalue="Agency Name or ID" value="Agency Name or ID" />
                                    <input type="hidden" id="hidtxtAgencyName" name="hidtxtAgencyName" value="" />
                                </div>
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-md-3">
                                Total Amount :&nbsp;&nbsp;
                                <asp:Label ID="lbl_Total" runat="server"></asp:Label>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                Total_Ticket_Count :&nbsp;&nbsp;&nbsp;&nbsp;
                                <asp:Label ID="lbl_Sales" runat="server"></asp:Label>
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <br />
                                    <asp:Button ID="btn_result" runat="server" Text="Search Result" CssClass="btn btn-lg btn-primary" OnClick="btn_result_Click" />
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <br />
                                    <asp:Button ID="btn_export" runat="server" Text="Export" CssClass="btn btn-lg btn-primary" OnClick="btn_export_Click" />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div style="color: #FF0000">
                                * N.B: To get Today's booking without above parameter,do not fill any field, only
                            click on search your booking.
                            </div>
                        </div>
                        <hr />
                        <div class="row" style="background-color: #fff; overflow: auto;" runat="server">
                            <div align="center">
                                <table width="100%" border="0" cellpadding="0" cellspacing="0" style="background-color: white;">
                                    <tr>
                                        <td>
                                            <asp:UpdatePanel ID="up" runat="server">
                                                <ContentTemplate>
                                                    <asp:GridView ID="GrdBusReport" runat="server" AllowPaging="true" AutoGenerateColumns="false"
                                                        CssClass="table" GridLines="None" PageSize="30">
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="Booking">
                                                                <ItemTemplate>

                                                                    <a href='Update_BookingOrder.aspx?OrderId=<%#Eval("OrderId")%> &TransID='
                                                                        rel="lyteframe" rev="width: 1200px; height: 500px; overflow:hidden;" target="_blank"
                                                                        style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold; color: #004b91">Booking&nbsp;Details
                                                                    </a>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="ORDERID">
                                                                <ItemTemplate>
                                                                    <a href='../BS/TicketSummary.aspx?tin=<%#Eval("TICKETNO")%>&oid=<%#Eval("ORDERID")%>' class="gridViewToolTip"
                                                                        rel="lyteframe" rev="width: 1000px; height: 500px; overflow:hidden;" target="_blank"
                                                                        style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold; color: #668aff">
                                                                        <asp:Label ID="TKTNO" runat="server" Text='<%#Eval("ORDERID")%>'></asp:Label>
                                                                    </a>
                                                                    <div id="tooltip" style="display: none;">
                                                                        <div style="float: left;">
                                                                            <table width="100%" cellpadding="11" cellspacing="11" border="0">
                                                                                <tr>
                                                                                    <td style="width: 110px; font-weight: bold;">Refund_Request:
                                                                                    </td>
                                                                                    <td style="width: 166px;">
                                                                                        <%#Eval("CREATED_DATE")%>  
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td style="width: 110px; font-weight: bold;">Accepted:
                                                                                    </td>
                                                                                    <td style="width: 166px;">
                                                                                        <%#Eval("ACCEPTEDDATE")%>  
                                                                                    </td>
                                                                                </tr>

                                                                                <tr>
                                                                                    <td style="width: 110px; font-weight: bold;">Refunded:
                                                                                    </td>
                                                                                    <td style="width: 166px;">
                                                                                        <%#Eval("REFUNDEDDATE")%>  
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </div>
                                                                    </div>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:BoundField DataField="AGENTID" HeaderText="Agent&nbsp;Id" />
                                                            <asp:BoundField DataField="AGENCY_NAME" HeaderText="Agency&nbsp;Name" />
                                                            <asp:BoundField DataField="TRIPID" HeaderText="Trip&nbsp;Id" />
                                                            <asp:BoundField DataField="SOURCE" HeaderText="Source" />
                                                            <asp:BoundField DataField="DESTINATION" HeaderText="Destination" />
                                                            <asp:BoundField DataField="BUSOPERATOR" HeaderText="Bus&nbsp;Operator" />
                                                            <asp:BoundField DataField="SEATNO" HeaderText="Seat&nbsp;No" />
                                                            <asp:BoundField DataField="TA_NET_FARE" HeaderText="Fare" />
                                                            <asp:BoundField DataField="TICKETNO" HeaderText="Ticket&nbsp;No" />
                                                            <asp:BoundField DataField="PNR" HeaderText="PNR" />
                                                            <asp:BoundField DataField="BOOKINGSTATUS" HeaderText="Booking&nbsp;Status" />
                                                            <asp:BoundField DataField="JOURNEYDATE" HeaderText="Journey&nbsp;Date" />
                                                            <asp:BoundField DataField="CREATEDDATE" HeaderText="Booking&nbsp;Date" />
                                                            <asp:BoundField DataField="Paymentmode" HeaderText="PaymentMode" />
                                                            <asp:BoundField DataField="Status" HeaderText="CurrentStatus" />
                                                        </Columns>
                                                        <RowStyle CssClass="RowStyle" />
                                                        <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                                                        <PagerStyle CssClass="PagerStyle" />
                                                        <SelectedRowStyle CssClass="SelectedRowStyle" />
                                                        <HeaderStyle CssClass="HeaderStyle" />
                                                        <EditRowStyle CssClass="EditRowStyle" />
                                                        <AlternatingRowStyle CssClass="AltRowStyle" />
                                                    </asp:GridView>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </td>
                                    </tr>
                                </table>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        function validate() {
            if (confirm("Are you sure you want to change status to hold!")) {
                return true;
            } else {
                return false;
            }
        }
        function closethis() {
            $("#outerdiv").hide();
        }

        $(function () {
            InitializeToolTip();
        });
    </script>
    <script type="text/javascript">
        var UrlBase = '<%=ResolveUrl("~/") %>';
    </script>

    <link href="<%=ResolveUrl("~/Hotel/css/B2Bhotelengine.css") %>" rel="stylesheet" type="text/css" />
</asp:Content>

