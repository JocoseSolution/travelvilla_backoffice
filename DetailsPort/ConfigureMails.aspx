﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ConfigureMails.aspx.vb" Inherits="DetailsPort_ConfigureMails" MasterPageFile="~/MasterPageSignIn.master" %>

<%@ Register Src="~/UserControl/LeftMenu.ascx" TagPrefix="uc1" TagName="LeftMenu" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <%-- <link href="../../css/StyleSheet.css" rel="stylesheet" type="text/css" />
    <link href="../../CSS/main2.css" rel="stylesheet" type="text/css" />
    <link href="Styles/tables.css" rel="stylesheet" type="text/css" />
    <link href="../../CSS/style.css" rel="stylesheet" type="text/css" />
    <%--<link href="../../CSS/style.css" rel="stylesheet" type="text/css" />--%>
    <link type="text/css" href="<%=ResolveUrl("~/Styles/jquery-ui-1.8.8.custom.css") %>" rel="stylesheet" />


    <style type="text/css">
        .page-wrapperss {
            background-color: #fff;
            margin-left: 15px;
        }

        .overfl {
            overflow: auto;
        }
    </style>
    <div class="row">
        <div class="container-fluid" style="padding-right: 35px">
            <div class="page-wrapperss">

                <div class="panel panel-primary">

                    <div class="panel-body">

                        <div class="row">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <%--<label for="exampleInputPassword1"></label>--%>
                                    <asp:DropDownList ID="ddl_airline" runat="server" placeholder="Module Type" CssClass="input-text full-width">
                                        <asp:ListItem Value="">--select--</asp:ListItem>
                                        <asp:ListItem Value="Hold">Hold</asp:ListItem>
                                        <asp:ListItem Value="Reissue">Reissue</asp:ListItem>
                                        <asp:ListItem Value="Refund">Refund</asp:ListItem>
                                        <asp:ListItem Value="Failed">Failed</asp:ListItem>
                                        <asp:ListItem Value="GroupBooking">GroupBooking</asp:ListItem>

                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <%--<label for="exampleInputPassword1"></label>--%>
                                    <asp:DropDownList ID="ddlModuleType" runat="server" placeholder="Module Type" CssClass="input-text full-width">
                                        <asp:ListItem Value="">-----Select-----</asp:ListItem>
                                        <asp:ListItem Value="Hold">Hold</asp:ListItem>
                                        <asp:ListItem Value="Reissue">Reissue</asp:ListItem>
                                        <asp:ListItem Value="Refund">Refund</asp:ListItem>
                                        <asp:ListItem Value="Failed">Failed</asp:ListItem>
                                        <asp:ListItem Value="GroupBooking">GroupBooking</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="input-group">
                                    <%--<label for="exampleInputPassword1"></label>--%>
                                    <asp:TextBox ID="txtToEmail" Placeholder="Email" Class="form-control input-text full-width" runat="server"></asp:TextBox>
                                    <span class="input-group-addon" style="background-color: #49cced">
                                        <span class="fa fa-envelope"></span>
                                         </span>
                                </div>
                            </div>

                            <%-- <div class="col-md-4">
                                <div class="form-group">
                                      <label for="exampleInputPassword1">Is Active</label>
                        <asp:RadioButtonList ID="rbtnIsActiveTrue" runat="server" GroupName="grpIsAvtive" Checked="true" CssClass="radioButtonList"  RepeatDirection="Horizontal"/>
            <asp:RadioButton ID="rbtnIsActiveFalse" runat="server"  Checked="true" CssClass="radioButtonList"  RepeatDirection="Horizontal"/>
                                </div>--%>


                            
                             <div class="col-md-2">
                                <div class="form-inline">
                                    <label for="exampleInputPassword1">Is Active</label>
                                    <br />
                                    <asp:RadioButton ID="rbtnIsActiveTrue" RepeatColumns="2"  RepeatDirection="Horizontal" Text="Yes" runat="server" GroupName="grpIsAvtive" Checked="true"/>
                                    <asp:RadioButton ID="rbtnIsActiveFalse" RepeatLayout="Flow" RepeatDirection="Horizontal" Text="No" runat="server" GroupName="grpIsAvtive" />
                                    
                                </div>
                            </div>
                                
                           <%-- <asp:RadioButtonList ID="grpIsAvtive" runat="server" RepeatDirection="Horizontal"
                                Width="93px" RepeatLayout="Flow">
                                <asp:ListItem Text="Or" Value="OR" />
                                <asp:ListItem Text="And" Value="AND" Selected="True" />
                            </asp:RadioButtonList>--%>


                             <div class="col-md-4">
                                <div class="form-group">

                                    <asp:Button ID="btnConfigureMail" runat="server" Text="Submit" CssClass="btn btn-success" OnClientClick="return configValidation();" />
                                </div>
                            </div>
                        </div>
                            <%--<hr />--%>
                            <div class="row">

                                <div class="col-md-12">

                                    <div id="divReport" runat="server" visible="false">
                                        <%-- style="overflow: scroll; max-height: 250px; width: 100%; float: left;"--%>
                                        <asp:GridView ID="GridView1" runat="server" AllowPaging="True" AllowSorting="True"
                                            AutoGenerateColumns="False" CssClass="table" GridLines="None" PageSize="10" OnPageIndexChanging="GridView1_PageIndexChanging" style="text-transform:uppercase;">
                                            <Columns>
                                                <asp:TemplateField HeaderText="Sno" Visible="false">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl_Sno" runat="server" Text='<%#Eval("Sno")%>'></asp:Label>(View)</a>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Module">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl_Module" runat="server" Text='<%#Eval("ModuleType") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Email">
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="txt_ToEmail" runat="server" Text='<%#Eval("ToEmail") %>'></asp:TextBox>

                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl_ToEmail" runat="server" Text='<%#Eval("ToEmail") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Active/Deactive">
                                                    <EditItemTemplate>
                                                        <asp:DropDownList ID="ddlActDact" runat="server" SelectedValue='<%#Eval("IsActive")%>'>
                                                            <asp:ListItem Value="True">Activate</asp:ListItem>
                                                            <asp:ListItem Value="False">Deactivate</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl_IsActive" runat="server" Text='<%#Eval("IsActive")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Created By">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl_CreatedBy" runat="server" Text='<%#Eval("CreatedBy")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Created Date">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl_CreatedDate" runat="server" Text='<%#Eval("CreatedDate")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Updated By">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl_UpdatedBy" runat="server" Text='<%#Eval("UpdatedBy")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Updated Date">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl_UpdatedDate" runat="server" Text='<%#Eval("UpdatedDate")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Edit/Delete" ItemStyle-CssClass="nowrapgrdview">
                                                    <EditItemTemplate>
                                                        <asp:LinkButton ID="lbtnUpdate" runat="server" CommandName="Update" Text="Update" CommandArgument='<%#Eval("Sno")%>' OnClientClick="return confirmUpdate(this);"></asp:LinkButton>
                                                        <asp:LinkButton ID="lbtnCancel" runat="server" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lbtnEdit" runat="server" CommandName="Edit" Text="Edit"></asp:LinkButton>/
                                    <asp:LinkButton ID="lbtnDelete" runat="server" CommandName="Delete" Text="Delete" CommandArgument='<%#Eval("Sno")%>' OnClientClick="return confirmDelete();"></asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <RowStyle CssClass="RowStyle" />
                                            <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                                            <PagerStyle CssClass="PagerStyle" />
                                            <SelectedRowStyle CssClass="SelectedRowStyle" />
                                            <HeaderStyle CssClass="HeaderStyle" />
                                            <EditRowStyle CssClass="EditRowStyle" />
                                            <AlternatingRowStyle CssClass="AltRowStyle" />
                                        </asp:GridView>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <script type="text/javascript">
            function confirmUpdate(thisObj) {
                var txtEm;
                var objtoFoc;
                var cntEm = 0;
                var thisObjRow = $(thisObj).parent().parent();
                var txtEmObj = $($(thisObjRow).find("td")[1]).find("input[type='text']");


                if ($.trim($(txtEmObj).val()) == "") {
                    alert("Email is required");
                    $("#" + $.trim($(txtEmObj).attr("id"))).focus();
                    return false;
                }
                else {
                    if (/^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/.test($.trim($(txtEmObj).val()))) {
                        //alert("hi")
                        var upd = confirm('Are you sure to update this configuration');
                        if (upd == true) {
                            return true;
                        }
                        else {
                            return false;
                        }
                        return true;
                    }
                    else {
                        alert("invalid email");
                        $("#" + $.trim($(txtEmObj).attr("id"))).focus();
                        return false;
                    }

                }
            }
            function confirmDelete() {
                var upd = confirm('Are you sure to delete this configuration');
                if (upd == true) {
                    return true;
                }
                else {
                    return false;
                }
            }

            function configValidation() {
                if ($.trim($("#<%=ddlModuleType.ClientID%>").val()) == "") {
                    alert("Module type is required");
                    $("#<%=ddlModuleType.ClientID%>").focus();
                    return false;
                }



                if ($.trim($("#<%=txtToEmail.ClientID%>").val()) == "") {
                    alert("Email are required");
                    $("#<%=txtToEmail.ClientID%>").focus();
                    return false;
                }


            }


        </script>
</asp:Content>
