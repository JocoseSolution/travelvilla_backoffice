﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageSignIn.master" AutoEventWireup="true" CodeFile="BlockAirlines.aspx.cs" Inherits="BlockAirlines" %>




<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
   
    <script type="text/javascript">

    </script>


    <script src="<%=ResolveUrl("~/Js/jquery-ui-1.8.8.custom.min.js") %>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Js/jquery-1.7.1.min.js") %>" type="text/javascript"></script>
    <script src="validation.js"></script>

    <div>
        <div class="container-fluid" style="padding-right: 35px">

            <div>
                <div class="panel panel-primary">
                    <div class="panel-body">
                        <div class="widget-content nopadding">

                            <div id="form-wizard-1" class="step">
                                <div class="row">
                                    <div class="col-md-2">
                                        <div class="input-group">
                                            <asp:DropDownList ID="ddl_Pairline" CssClass="input-text full-width" runat="server">
                                            </asp:DropDownList>



                                        </div>
                                    </div>
                               

                                    <div class="col-xs-2">
                                        <div class="input-group">
                                      <asp:DropDownList ID="DdlTripType" runat="server" CssClass="input-text full-width">
                                        <asp:ListItem Value="SELECT"> SELECT TRIP</asp:ListItem>
                                        <asp:ListItem Value="D" Text="Domestic"></asp:ListItem>
                                        <asp:ListItem Value="I" Text="International"></asp:ListItem>
                                    </asp:DropDownList>
                                        </div>
                                    </div>


                                     <div class="col-xs-2">
                                        <div class="input-group">
                                      <asp:DropDownList ID="statusddl" runat="server" CssClass="input-text full-width">
                                        <asp:ListItem Value="SELECT">STATUS</asp:ListItem>
                                        <asp:ListItem Value="FALSE" Text="FALSE"></asp:ListItem>
                                        <asp:ListItem Value="TRUE" Text="TRUE"></asp:ListItem>
                                    </asp:DropDownList>
                                        </div>
                                    </div>


                                    
                              <div class="col-xs-2" style="margin-top:5px">
                                        <div class="form-group">
                                            <asp:Button ID="Submit" runat="server" Text="Submit" CssClass="btn btn-success" OnClick="Submit_Click" ValidationGroup="group1" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                </div>

                                <hr />
                                <div>
                                    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="false" DataKeyNames="ID"
                                        OnRowEditing="OnRowEditing" OnRowCancelingEdit="OnRowCancelingEdit"
                                        OnRowUpdating="OnRowUpdating" OnRowDeleting="OnRowDeleting" PageSize="8"
                                     CssClass="table table-striped table-bordered table-hover"   Width="60%" style="text-transform:uppercase;">

                                        <Columns>
                                            <%--<asp:TemplateField HeaderText="Counter" ItemStyle-Width="150">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblcounter" runat="server" Text='<%# Eval("counter") %>'></asp:Label>
                                                </ItemTemplate>

                                            </asp:TemplateField>--%>
                                            <asp:TemplateField HeaderText="Airline" ItemStyle-Width="150">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblAirline" runat="server" Text='<%# Eval("Airline") %>'></asp:Label>
                                                </ItemTemplate>

                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Trip" ItemStyle-Width="150">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblTrip" runat="server" Text='<%# Eval("Trip") %>'></asp:Label>
                                                </ItemTemplate>

                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Status" ItemStyle-Width="150">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblstatus" runat="server" Text='<%# GetStatusVal( Eval("ISACTIVE")) %>'></asp:Label>
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <asp:DropDownList ID="ddlstatus" runat="server" Width="150px" SelectedValue='<%# GetStatusVal(Eval("ISACTIVE")) %>'>
                                                        <asp:ListItem Value="TRUE" Text="TRUE"></asp:ListItem>
                                                        <asp:ListItem Value="FALSE" Text="FALSE"></asp:ListItem>
                                                    </asp:DropDownList>
                                                </EditItemTemplate>

                                            </asp:TemplateField>


                                            <asp:TemplateField HeaderText="Edit/Delete" ItemStyle-CssClass="nowrapgrdview">
                                                <EditItemTemplate>
                                                    <asp:LinkButton ID="lbtnUpdate" runat="server" CommandName="Update" Text="Update"></asp:LinkButton>
                                                    <asp:LinkButton ID="lbtnCancel" runat="server" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                                                </EditItemTemplate>
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lbtnEdit" runat="server" CommandName="Edit" Text="Edit"></asp:LinkButton>/
                                                    <asp:LinkButton ID="lbtnDelete" runat="server" CommandName="Delete" Text="Delete" CommandArgument='<%#Eval("id")%>' OnClientClick="return confirmDelete();"></asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>


                                        </Columns>

                                    </asp:GridView>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



</asp:Content>

