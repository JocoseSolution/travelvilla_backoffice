﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;


public partial class BlockAirlines : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString);
    private SqlTransactionDom STDom = new SqlTransactionDom();
    private SqlDataAdapter adap;

    protected void Page_Load(object sender, EventArgs e)
    {
       

        if (Session["UID"] == null)
        {
            Response.Redirect("~/Login.aspx");
        }

        try
        {
            if (!IsPostBack)
            {
                ddl_Pairline.AppendDataBoundItems = true;
                ddl_Pairline.Items.Clear();
                ddl_Pairline.Items.Insert(0, new ListItem("SELECT", "SELECT AIRLINE"));
                ddl_Pairline.DataSource = GetAirline();
                ddl_Pairline.DataTextField = "AL_Name";
                ddl_Pairline.DataValueField = "AL_Code";
                ddl_Pairline.DataBind();
                //ddl_Pairline.Items.Add(new ListItem("All Type", "All Type"));



                BindGridview();
            }


        }
        catch (Exception ex)
        {
        }
    }

   
    public string GetStatusVal(object val)
    {
        string value = "";
        bool result = Convert.ToBoolean(val);

        if (result == false)
        {
            value = "FALSE";
        }
        else
        {
            value = "TRUE";
        }
        return value;
    }
    protected void Submit_Click(object sender, EventArgs e)
    {

      
        //int result = 0;
        string result = "";

        try
        {
            if (ddl_Pairline.SelectedValue == "SELECT" || DdlTripType.SelectedValue == "SELECT" || statusddl.SelectedValue == "SELECT")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Please Select all fields');", true);
            }
            else
            {
                result = insertdata();
                if (result == "Insert")
                {
                    BindGridview();

                    ddl_Pairline.SelectedIndex = 0;
                    DdlTripType.SelectedIndex = 0;
                    statusddl.SelectedIndex = 0;
                    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Record Inserted successfully.');", true);
                }
                else
                {
                    BindGridview();
                    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Record Already Exists.');", true);

                }
            }
        }
       
        catch (Exception ex)
        {
            throw ex;
        }
    }


    protected void BindGridview()
    {
        string constr = ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString;
        SqlConnection con = new SqlConnection(constr);
        con.Open();

        SqlCommand cmd = new SqlCommand("BLOCKLISTUPDATE_MPP");
        cmd.Parameters.AddWithValue("@ACTION", "SELECT");
        cmd.CommandType = CommandType.StoredProcedure;
        SqlDataAdapter sda = new SqlDataAdapter();

        cmd.Connection = con;
        sda.SelectCommand = cmd;
        DataTable dt = new DataTable();

        sda.Fill(dt);
        con.Close();
        GridView1.DataSource = dt;

        GridView1.DataBind();

    }

    protected void OnRowCancelingEdit(object sender, EventArgs e)
    {
        GridView1.EditIndex = -1;
        this.BindGridview();

    }

    protected void OnRowEditing(object sender, GridViewEditEventArgs e)
    {
        GridView1.EditIndex = e.NewEditIndex;
        this.BindGridview();
    }

    protected void OnRowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        string ID = GridView1.DataKeys[e.RowIndex].Values[0].ToString();
        string constr = ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString;
        using (SqlConnection con = new SqlConnection(constr))
        {
            using (SqlCommand cmd = new SqlCommand("BLOCKLISTUPDATE_MPP"))
            {
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@ID", ID);
                cmd.Parameters.AddWithValue("@ACTION","DELETE");
                
                cmd.Connection = con;
                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();
            }
        }
        this.BindGridview();

    }


    protected void OnRowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        GridViewRow row = GridView1.Rows[e.RowIndex];
        string id = GridView1.DataKeys[e.RowIndex].Values[0].ToString();
        string status = (row.FindControl("ddlstatus") as DropDownList).SelectedItem.ToString();

        string constr = ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString;
        using (SqlConnection con = new SqlConnection(constr))
        {
            using (SqlCommand cmd = new SqlCommand("BLOCKLISTUPDATE_MPP"))
            {
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@ID", id);
                cmd.Parameters.AddWithValue("@ACTION","UPDATE");
                cmd.Parameters.AddWithValue("@ISACTIVE", status.ToUpper() == "TRUE" ? 1 : 0);


                cmd.Connection = con;
                con.Open();
                int i = Convert.ToInt32(cmd.ExecuteNonQuery());
                con.Close();
                if(i > 0)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Record updated successfully.');", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Record not updated.');", true);
                }
                
            }
        }
        GridView1.EditIndex = -1;
        this.BindGridview();

    }

   

    //mk
    public DataTable GetAirline()
    {
        DataTable dt = new DataTable();
        try
        {
            adap = new SqlDataAdapter("SP_GetAirlinenames", con);
            adap.SelectCommand.CommandType = CommandType.StoredProcedure;
            adap.Fill(dt);
        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
        }
        finally
        {
            adap.Dispose();
        }
        return dt;
    }

    public string insertdata()
    {
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString);
      
        try
        {
            con.Open();
            SqlCommand cmd = new SqlCommand("BLOCKLIST_MPP", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@AIRCODE",ddl_Pairline.SelectedValue.Trim());
            cmd.Parameters.AddWithValue("@AIRLINE", ddl_Pairline.SelectedItem.Text.Trim());
            cmd.Parameters.AddWithValue("@TRIP", DdlTripType.SelectedValue.Trim());
            cmd.Parameters.AddWithValue("@ISACTIVE",statusddl.SelectedValue.ToUpper() == "TRUE" ? 1 : 0);


            string ret = "";
            ret = cmd.ExecuteScalar().ToString();
            return ret;

        }
        catch (Exception info)
        {
            throw info;
        }
        finally
        {
            con.Close();
            con.Dispose();
        }


    }

}