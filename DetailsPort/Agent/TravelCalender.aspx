﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPageSignIn.master" AutoEventWireup="false"
    CodeFile="TravelCalender.aspx.vb" Inherits="DetailsPort_Agent_TravelCalender" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-1.4.4.min.js") %>"></script>

    <link href="<%=ResolveUrl("~/Styles/fullcalendar.css") %>" rel='stylesheet' />

    <script src="<%=ResolveUrl("~/Scripts/jquery-ui-1.10.2.custom.min.js") %>"></script>

    <script src="<%=ResolveUrl("~/Scripts/fullcalendar.min.js") %>"></script>

    <script type="text/javascript">

        $(document).ready(function() {
          
            var date = new Date();
            var d = date.getDate();
            var m = date.getMonth() ;
            var y = date.getFullYear();
            var m1 = m + 1;
            var selectedvalue = '<%=  Session("UID") %>';
            GetCalender(selectedvalue, y.toString() + "-" + m.toString() + "-1", m);
            var newMonth = date.getMonth();

            //        $('#calendar').fullCalendar({
            //            header: {
            //                // left:''// 'prev today',
            //                center: 'title'//,
            //                //  right:'' //'next'
            //            },
            //            editable: true
            //        });

            $('#PrevButton').attr("disabled", true);

            $('#NextButton').click(function() {
                var compDate = new Date();

                newMonth = newMonth + 1;
                var newDate = new Date(y, newMonth, 1);
                var newmonth1 = newMonth + 1;
                var newDate1 = new Date(y, newMonth, compDate.getDate());
                //alert(newDate);
                // $('#calendar').fullCalendar('destroy');
                // $('#calendar').fullCalendar('next');
                GetCalender(selectedvalue, y.toString() + "-" + newMonth.toString() + "-1", newMonth);
                if (newDate1.toDateString() == compDate.toDateString()) {
                    // alert(compDate);
                    $('#PrevButton').attr("disabled", true);

                }
                else {
                    $('#PrevButton').attr("disabled", false);
                }



            });

            $('#PrevButton').click(function() {
                var compDate = new Date();


                newMonth = newMonth - 1;
                var newDate = new Date(y, newMonth, 1);
                var newDate1 = new Date(y, newMonth, compDate.getDate());
                // alert(newDate1.toDateString());
                // alert(compDate.toDateString());

                //alert(compDate.toDateString());
                //alert(newDate1.toDateString());

                //alert(newDate);
                // $('#calendar').fullCalendar('destroy');
                // $('#calendar').fullCalendar('next');
                GetCalender(selectedvalue, y.toString() + "-" + newMonth.toString() + "-1", newMonth);
                if (newDate1.toDateString() == compDate.toDateString()) {

                    $('#PrevButton').attr("disabled", true);
                }
                else {
                    $('#PrevButton').attr("disabled", false);

                }

            });


        });



        function GetCalender(agentId, curentdate, month1) {

            var yr = new Date().getFullYear();

            var validDate = curentdate;
            if (month1 >= 12) {
                if (month1 == 12) {
                    //                month1 = parseInt(month1 - 11);
                    //                yr = parseInt(yr + 1);

                    //                validDate = yr.toString() + "-" + month1.toString() + "-1"
                }
                else {
                    month1 = parseInt(month1 - 12);
                    yr = parseInt(yr + 1);

                    validDate = yr.toString() + "-" + month1.toString() + "-1"
                }

            }
            // alert(validDate);
            //            validDate = yr.toString() + "-" + parseInt(month1 + 1).toString() + "-1"
//            var validdate = validDate.split('-');
//            if (parseInt(validdate[1]) == 0) {

//                validDate = validdate[0] + "-" + (parseInt(validdate[1]) + 1).toString() + "-" + validdate[2];
//            }
            
            var eventDataArray = new Array();


            $.ajax({
            //Absolute Url name TravelCalender.aspx
                            url: "../../AgencySearch.asmx/GetFlightDetails",
            //url: "TravelCalender.aspx/GetFlightDetails",
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                data: "{'agentID':' " + agentId + "','date1':'" + validDate + "'}",
                success: function(data) {

                    for (var i = 0; i < data.d.length; i++) {
                        var date = convertToDate(data.d[i].DepDate);
                        if (data.d[i].Sector == "more") {

                            // alert(data.d[i].Sector);
                            var mon = date.getMonth() + 1;
                            eventDataArray.push({
                                title: 'MORE...',
                                start: date,
                                link: date.getFullYear().toString() + "-" + mon.toString() + "-" + date.getDate().toString()


                            });

                        }
                        else {

                            eventDataArray.push({
                                title: "DepTime: " + data.d[i].DepTime.splice(2, 0, ":") + "Hrs" + "\n   " + data.d[i].OrderID + "\n  Sector: " + data.d[i].Sector,
                                start: date,
                                link: "../../DetailsPort/PnrSummaryIntl.aspx?OrderId=" + data.d[i].OrderID + " &TransID="


                            });
                        }

                        //                    if (i > 0) {
                        //                        if (data.d[i - 1].DepDate != data.d[i].DepDate) {
                        //                            if (i < data.d.length) {
                        //                                if (data.d[i].DepDate == data.d[i + 1].DepDate) {
                        //                                    alert(data.d[i].DepDate + ", " + data.d[i + 1].DepDate);
                        //                                    var date1 = convertToDate(data.d[i - 1].DepDate);
                        //                                    var mon = date1.getMonth() + 1;
                        //                                    eventDataArray.push({
                        //                                        title: 'MORE...',
                        //                                        start: date1,
                        //                                        link: date1.getFullYear().toString() + "-" + mon.toString() + "-" + date1.getDate().toString()


                        //                                    });

                        //                                }

                        //                            }



                        //                        }

                        //                    }


                    }


                    $('#calendar').fullCalendar('destroy');
                    $('#calendar').fullCalendar({
                        header: {
                            left: '  ',
                            center: 'title',
                            right: ' '
                        },
                        year: yr,
                        month: month1,
                        events: eventDataArray,
                        eventClick: function(calEvent, jsEvent, view) {

                            window.open(calEvent.link, "_blank");
                            // alert('Event: ' + calEvent.link);
                            // alert('Coordinates: ' + jsEvent.pageX + ',' + jsEvent.pageY);
                            //   alert('View: ' + view.name);

                            // change the border color just for fun
                            //$(this).css('border-color', 'red');

                        },
                        eventMouseover: function(calEvent, jsEvent, view) {
                            if (calEvent.title == "MORE...") {

                                GetToolTip(agentId, calEvent.link, jsEvent.pageY - 150, jsEvent.pageX - 100);
                            }

                            // window.open(calEvent.link, "_blank");
                            // alert('Event: ' + calEvent.link);
                            // alert('Coordinates: ' + jsEvent.pageX + ',' + jsEvent.pageY);
                            //   alert('View: ' + view.name);

                            // change the border color just for fun
                            //$(this).css('border-color', 'red');

                        }


                    });






                },
                error: function(XMLHttpRequest, textStatus, errorThrown) {
                    $('#calendar').fullCalendar('destroy');
                    //alert(textStatus);
                }

            });

        }

        function convertToDate(DepDate) {

            var d = DepDate.substring(0, 2);
            var m = DepDate.substring(2, 4);
            var y = "20" + DepDate.substring(4, 6);
            var date = new Date(y, m - 1, d);
            return date;

        }

        String.prototype.splice = function(idx, rem, s) {
            return (this.slice(0, idx) + s + this.slice(idx + Math.abs(rem)));
        };


        function GetToolTip(agentId, date, top, left) {






            //alert(document.location.href);
            //	            // alert("hi");
            //	            var existip = $("#tooltip");

            //	            if (existip != null) {
            //	                if ($("#tooltip").show() == true) {

            //	                    $("#tooltip").hide()
            //	                }
            //	                else {
            //	                    $("#tooltip").show()
            //	                }
            //	            }


            //   alert(param);
            // var param1 = param.split(',');
            //  alert(param1[0] + param1[1]);
            //$("#tooltip").css("display:none;");


            $.ajax({
                url: "../../AgencySearch.asmx/GetFlightDetailsByDateForCal",
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                data: "{'agentID':' " + agentId + "','date1':'" + date + "'}",

                success: function(data) {


                    var targetDiv = $('[rel~=tip2]');

                    if (targetDiv.length > 0) {
                        targetDiv.remove();


                    }

                    var targetDiv = $('[rel~=tip1]');

                    if (targetDiv.length > 0) {
                        targetDiv.remove();
                        // $('body').append('<div id="tooltip" rel="hi1"></div>');

                    }
                    //alert("hi");

                    var result = data.d;
                    var table = "";


                    ////table = "<a href='javascript:rem1();' style='color:#fff'>X</a><br/><table  width='300px' height='150px' cellpadding='0' cellspacing='0'>"
                    table = "<a href='javascript:rem1();' style='color:#000;font-size:14px;text-decoration:none;'><img src='../../Utility/img/basic/x.png' style='text-decoration:none;border:0;float:right;' /></a><table cellpadding='5' cellspacing='0'>"
                    table += "<tr style='font-size:14px;font-weight:bold;'><td>SNO</td><td>&nbsp;DEP TIME</td><td></td><td style='text-align:left;'>ORDERID</td><td></td><td>SECTOR</td></tr>"
                    for (var i = 0; i < result.length; i++) {

                        table += "<tr id='" + i + "'>";
                        table += "<td style='font-size:12px;font-weight:bold;' >" + parseInt(i + 1).toString() + "&nbsp;&nbsp;</td><td style='font-size:12px'>" + result[i].DepTime.splice(2, 0, ":") + "Hrs" + "</td><td>&nbsp;</td>";

                        table += "<td style='font-size:12px'><a target='_blank'  href=../../DetailsPort/PnrSummaryIntl.aspx?OrderId=" + data.d[i].OrderID + " &TransID=>" + result[i].OrderID + " </a></td></td><td>&nbsp;</td>";
                        table += "<td style='font-size:12px'>" + result[i].Sector + "<br/></td>";
                        table += "</tr>";
                    }
                    // table += "<tr><td align='left'><a href='PkgDetails.aspx?ID=" + param + "&price=" + result[0].PkgPrice + "#reviews' style='color:#fff;font-size:10px'>Read all reviews </a></td></tr>"
                    table += "</table>";

                    // alert(table);





                    tip = table; //"<a href='javascript:rem();'>X</a><br/><a href='PkgDetails.aspx?ID=" + param1[0] + "'>View Details</a>";



                    tooltip = $('<div id="tooltip"   rel="tip2"></div>');
                    if (!tip || tip == '')
                        return false;

                    //  target.removeAttr('alt');
                    tooltip.css('opacity', 0)
					   .html(tip)
					   .appendTo('body');

                    tooltip.focus();
                    var init_tooltip = function() {
                        // alert(e.pageY);
                        var yTop = 0;
                        if (window.event == null) {
                            // alert(e.pageY);
                            // yTop = e.pageY - 100;

                        }
                        else {
                            //yTop = window.event.clientY;
                        }
                        // yTop = window.event.clientY;
                        // alert(yTop);
                        // e = window.event || e;
                        if ($(window).width() < tooltip.outerWidth() * 1.5)
                            tooltip.css('max-width', $(window).width() / 2);
                        else
                            tooltip.css('max-width', 340);
                        var top1 = null;
                        var ad = null;
                        if (top > yTop) {
                            if (yTop > 200) {
                                top1 = top;
                                // ad = -10;

                            }
                            else {

                                top1 = yTop - top - 100;
                                // ad = -10;

                            }
                        }
                        else {

                            top1 = top;
                        }
                        if (top > 200) {
                            if (/MSIE (\d+\.\d+);/.test(navigator.userAgent)) {
                                ad = 35;
                            }
                            else {
                                ad = 30;
                            }

                        }
                        else {
                            if (/MSIE (\d+\.\d+);/.test(navigator.userAgent)) {
                                ad = -2;
                            }
                            else {
                                ad = 0;
                            }

                        }


                        var pos_left = left + 100 - (tooltip.outerWidth() / 2),
						   pos_top = top - 10 - 20;

                        if (pos_left < 0) {
                            pos_left = left + 100 - 15;
                            tooltip.addClass('left');
                        }
                        else
                            tooltip.removeClass('left');

                        if (pos_left + tooltip.outerWidth() > $(window).width()) {
                            pos_left = left - tooltip.outerWidth() + target.outerWidth() / 2 + 20;
                            tooltip.addClass('right');
                        }
                        else
                            tooltip.removeClass('right');

                        if (pos_top < 0) {

                            var pos_top = top + 20;
                            tooltip.addClass('top');
                        }
                        else
                            tooltip.removeClass('top');
                        if (/MSIE (\d+\.\d+);/.test(navigator.userAgent)) {
                            if (pos_top < 200) {
                                if (window != null)

                                    window.scrollTo(left, 0);
                                // alert(e.pageY/2);

                                // tooltip.scrollTop();


                            }

                            else {

                                window.scrollTo(left, top - 200);


                            }

                        }

                        tooltip.css({ left: pos_left, top: pos_top })
						   .animate({ top: '+=' + ad, opacity: 1 }, 50);

                    };

                    init_tooltip();
                    $(window).resize(init_tooltip);

                    var remove_tooltip = function() {
                        tooltip.animate({ top: '-=10', opacity: 0 }, 50, function() {
                            $(this).remove();
                        });

                        //target.attr('alt', param);
                    };


                    // target.bind('mouseover', call);
                    // target.bind('mouseover', call_tooltip);
                    // target.bind('mouseout', remove_tooltip);
                    tooltip.bind('mouseleave', remove_tooltip);
                    //target.bind('mouseleave', remove_tooltip);
                    //alert(target.parent());



                }

            });





        }


        function rem1() {


            $("#tooltip").remove();
        }

    </script>

    <style>
        body
        {
            margin-top: 40px;
            text-align: center;
            font-size: 14px;
            font-family: "Lucida Grande" ,Helvetica,Arial,Verdana,sans-serif;
        }
        #calendar
        {
            width: 900px;
            margin: 0 auto;
        }
        #tooltip
        {
            /*font-size: 0.9em;*/
            font-size: 12px;
            font-family: Calibri,arial;
            border: 2px solid #004b91;
            text-align: left;
            text-shadow: 0 1px rgba( 0, 0, 0, .5 );
            line-height: 1.5;
            background: #FFF;
            -webkit-border-radius: 5px;
            -moz-border-radius: 5px;
            border-radius: 5px;
            -webkit-box-shadow: 0 3px 5px rgba( 0, 0, 0, .3 );
            -moz-box-shadow: 0 3px 5px rgba( 0, 0, 0, .3 );
            box-shadow: 0 3px 5px rgba( 0, 0, 0, .3 );
            position: absolute;
            z-index: 100;
            padding: 15px;
            padding: 3px 4px 15px 15px;
        }
        #tooltip:after
        {
            width: 0;
            height: 0;
            border-left: 10px solid transparent;
            border-right: 10px solid transparent;
            border-top-color: #333;
            border-top: 10px solid rgba( 0, 0, 0, .7 );
            content: '';
            position: absolute;
            left: 50%;
            bottom: -10px;
            margin-left: -10px;
        }
        #tooltip.top:after
        {
            border-top-color: transparent;
            border-bottom-color: #333;
            border-bottom: 10px solid rgba( 0, 0, 0, .6 );
            top: -20px;
            bottom: auto;
            margin: 0;
            padding: 0;
        }
        #tooltip.left:after
        {
            left: 10px;
            margin: 0;
        }
        #tooltip.right:after
        {
            right: 10px;
            left: auto;
            margin: 0;
        }
    </style>
    <table cellspacing="10" cellpadding="0" border="0" align="center" class="tbltbl">
        <tr>
            <td align="left">
                <input type="button" id="PrevButton" value="prev" class="button" />
            </td>
            <td align="right">
                <input type="button" id="NextButton" value="next" class="button" />
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <div id='calendar'>
                </div>
            </td>
        </tr>
    </table>
</asp:Content>
