﻿Imports System.Data
Imports System.IO
Imports System.Net
Imports Ionic.Zip

Partial Class DetailsPort_Agent_Agent_Profile
    Inherits System.Web.UI.Page
    Dim STDom As New SqlTransactionDom

    Protected Sub LinkEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkEdit.Click
        Try
            td_login.Visible = False
            td_login1.Visible = True

        Catch ex As Exception
            clsErrorLog.LogInfo(ex)

        End Try
    End Sub

    Protected Sub lnk_Cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnk_Cancel.Click
        Try
            td_login1.Visible = False
            td_login.Visible = True
            lbl_msg.Text = ""

        Catch ex As Exception
            clsErrorLog.LogInfo(ex)

        End Try
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("UID") = "" Or Session("UID") Is Nothing Then
                Response.Redirect("~/Login.aspx")
            End If
            Dim dt As DataTable
            dt = STDom.GetAgencyDetails(Session("UID").ToString).Tables(0)
            Info()
            If Not IsPostBack Then
                txt_aemail.Text = dt.Rows(0)("Alt_Email").ToString()
                txt_fax.Text = dt.Rows(0)("Fax_no").ToString()
                txt_landline.Text = dt.Rows(0)("Phone").ToString()
                txt_address.Text = dt.Rows(0)("Address").ToString()
                txt_city.Text = dt.Rows(0)("City").ToString()
                txt_state.Text = dt.Rows(0)("State").ToString()
                txt_country.Text = dt.Rows(0)("Country").ToString()
            End If

            logo()


        Catch ex As Exception
            clsErrorLog.LogInfo(ex)

        End Try
    End Sub
    Public Sub logo()
        Try
            Dim filepath As String = Server.MapPath("~\AgentLogo") + "\" + Session("UID") + ".jpg" 'Server.MapPath("~/AgentLogo/" + LogoName)
            If (System.IO.File.Exists(filepath)) Then
                Image111.ImageUrl = "~/AgentLogo/" & Session("UID") & ".jpg"
            Else
                Image111.ImageUrl = "~/images/nologo.png"
            End If

        Catch ex As Exception
            clsErrorLog.LogInfo(ex)

        End Try
    End Sub
    Public Sub Info()
        Try


            Dim dtinfo As DataTable
            ' If Not IsPostBack Then
            dtinfo = STDom.GetAgencyDetails(Session("UID").ToString).Tables(0)
            td_username.InnerText = dtinfo.Rows(0)("user_id").ToString()
            td_Name.InnerText = dtinfo.Rows(0)("Title").ToString() & " " & dtinfo.Rows(0)("FName").ToString() & " " & dtinfo.Rows(0)("LName").ToString()
            td_EmailID.InnerText = dtinfo.Rows(0)("Email").ToString()
            td_Mobile.InnerText = dtinfo.Rows(0)("Mobile").ToString()
            td_Landline.InnerText = dtinfo.Rows(0)("Phone").ToString()
            td_Pan.InnerText = dtinfo.Rows(0)("PanNo").ToString()
            td_AlternateEmailID.InnerText = dtinfo.Rows(0)("Alt_Email").ToString()
            td_Fax.InnerText = dtinfo.Rows(0)("Fax_no").ToString()
            td_Add.InnerText = dtinfo.Rows(0)("Address").ToString()
            td_City.InnerText = dtinfo.Rows(0)("City").ToString()
            td_Country.InnerText = dtinfo.Rows(0)("Country").ToString()
            td_State.InnerText = dtinfo.Rows(0)("State").ToString()

            td_Name1.InnerText = dtinfo.Rows(0)("Title").ToString() & " " & dtinfo.Rows(0)("FName").ToString() & " " & dtinfo.Rows(0)("LName").ToString()
            td_Email1.InnerText = dtinfo.Rows(0)("Email").ToString()
            td_Mobile1.InnerText = dtinfo.Rows(0)("Mobile").ToString()
            td_Pan1.InnerText = dtinfo.Rows(0)("PanNo").ToString()
        Catch ex As Exception
            clsErrorLog.LogInfo(ex)

        End Try
    End Sub
    Protected Sub LinkEditAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkEditAdd.Click
        Try
            td_Address.Visible = False
            td_Address1.Visible = True

        Catch ex As Exception
            clsErrorLog.LogInfo(ex)

        End Try
    End Sub

    Protected Sub lnk_CancelAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnk_CancelAdd.Click
        Try
            td_Address.Visible = True
            td_Address1.Visible = False

        Catch ex As Exception
            clsErrorLog.LogInfo(ex)

        End Try
    End Sub

    Protected Sub LinkPersonalEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkPersonalEdit.Click
        Try
            td_PDetails.Visible = False
            td_PDetails1.Visible = True

        Catch ex As Exception
            clsErrorLog.LogInfo(ex)

        End Try
    End Sub

    Protected Sub lnk_CancelPDetails_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnk_CancelPDetails.Click
        Try
            td_PDetails.Visible = True
            td_PDetails1.Visible = False

        Catch ex As Exception
            clsErrorLog.LogInfo(ex)

        End Try
    End Sub

    Protected Sub button_upload_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles button_upload.Click
        Try
            Dim len As Integer = 0
            len = FileUpload1.PostedFile.ContentLength
            If len > 0 Then

                Dim finfo As New FileInfo(FileUpload1.FileName)
                Dim fileExtension As String = finfo.Extension.ToLower()
                If fileExtension <> ".jpg" Then
                    ClientScript.RegisterStartupScript(Page.GetType(), "Alert", "alert('Please Upload JPG formate');", True)
                Else

                    Dim file As String = ""
                    If FileUpload1.HasFile = True Then
                        Dim filepath As String = Server.MapPath("~/AgentLogo/" + Session("UID"))
                        filepath = filepath + ".jpg"
                        FileUpload1.SaveAs(filepath.ToString())
                        file = Path.GetFileName(Session("UID") + ".jpg")
                        logo()
                    End If

                End If
            Else
                ClientScript.RegisterStartupScript(Page.GetType(), "Alert", "alert('Please Select Image');", True)
            End If
        Catch ex As Exception
            clsErrorLog.LogInfo(ex)

        End Try
    End Sub


    Protected Sub btn_Save_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Save.Click
        Try
            If (txt_password.Text.Trim = txt_cpassword.Text.Trim) Then
                If txt_password.Text <> "" AndAlso txt_password.Text IsNot Nothing Then
                    STDom.UpdateAgentProfile("Login", Session("UID"), txt_password.Text.Trim, txt_aemail.Text.Trim, txt_landline.Text.Trim, txt_fax.Text.Trim, txt_address.Text.Trim, txt_city.Text.Trim, txt_state.Text.Trim, txt_country.Text.Trim)
                    td_login.Visible = True
                    td_login1.Visible = False
                    lbl_msg.Text = ""
                Else
                    lbl_msg.Text = "Enter Password"
                End If

            Else
                lbl_msg.Text = "Enter Same Password"
            End If

        Catch ex As Exception
            clsErrorLog.LogInfo(ex)

        End Try
    End Sub

    Protected Sub btn_SavePDetails_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_SavePDetails.Click
        Try
            STDom.UpdateAgentProfile("Details", Session("UID"), txt_password.Text.Trim, txt_aemail.Text.Trim, txt_landline.Text.Trim, txt_fax.Text.Trim, txt_address.Text.Trim, txt_city.Text.Trim, txt_state.Text.Trim, txt_country.Text.Trim)
            td_PDetails.Visible = True
            td_PDetails1.Visible = False
            Info()
        Catch ex As Exception
            clsErrorLog.LogInfo(ex)

        End Try
    End Sub

    Protected Sub btn_Saveadd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Saveadd.Click
        Try
            STDom.UpdateAgentProfile("Address", Session("UID"), txt_password.Text.Trim, txt_aemail.Text.Trim, txt_landline.Text.Trim, txt_fax.Text.Trim, txt_address.Text.Trim, txt_city.Text.Trim, txt_state.Text.Trim, txt_country.Text.Trim)
            td_Address.Visible = True
            td_Address1.Visible = False
            Info()
        Catch ex As Exception
            clsErrorLog.LogInfo(ex)

        End Try
    End Sub
    Protected Sub lnk_tds_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnk_tds.Click
        Try


            Dim Path As String
            Path = Server.MapPath("~/TDSPDF")
            Dim contains As String() = Directory.GetFiles(Path, "*" & td_Pan1.InnerText.ToUpper & "*", SearchOption.TopDirectoryOnly) '.IndexOf(Path, ".pdf")
            Dim zip As New ZipFile()
            If (contains.Length > 0) Then
                'Dim ik As Integer = 0
                zip.AlternateEncodingUsage = ZipOption.AsNecessary
                zip.AddDirectoryByName("TDSPDF")
                'For ik = 0 To contains.Length
                For Each d As String In contains
                    'Dim i As Integer = contains(d).IndexOf("TDSPDF")
                    'Dim i1 As Integer = contains(d).IndexOf(".pdf")

                    Dim i As Integer = d.IndexOf("TDSPDF")
                    Dim i1 As Integer = d.IndexOf(".pdf")

                    Dim Filename As String = d.Substring(i + 7, (i1 + 4) - (i + 7))
                    'Dim filePath As String = Server.MapPath("~/SampleFiles/" & Filename)
                    'zip.AddFile(filePath, "files")


                    ' zip.AddFile(Filename, "TDSPDF")
                    zip.AddFile(Server.MapPath("~/TDSPDF/" & Filename), "TDSPDF")
                Next

                ' ik = ik + 1

                'Next
                Try
                    Response.Clear()
                    Response.BufferOutput = False
                    Dim zipName As String = [String].Format("TDSCertificate_{0}.zip", DateTime.Now.ToString("yyyy-MMM-dd-HHmmss"))
                    Response.ContentType = "application/zip"
                    Response.AddHeader("content-disposition", "attachment; filename=" + zipName)
                    zip.Save(Response.OutputStream)
                    Response.[End]()
                    'Response.Clear()
                    'Response.AddHeader("Content-Disposition", "attachment; filename=DownloadedFile.zip")
                    'Response.ContentType = "application/zip"
                    'zip.Save(Response.OutputStream)
                    'Response.[End]()


                    'Response.Clear()
                    'Response.ContentType = "application/pdf"
                    'Response.AppendHeader("Content-Disposition", "attachment; filename=TDSCertificate.pdf")
                    'Response.TransmitFile("~/TDSPDF/" & Filename & "")
                    '' Response.End()
                Catch ex As Exception

                End Try
            Else
                ClientScript.RegisterStartupScript(Page.GetType(), "Alert", "alert('TDS Certificate not found. Please contact to account department');", True)
            End If
        Catch ex As Exception
            clsErrorLog.LogInfo(ex)

        End Try
    End Sub
    Public Sub TheDownload(ByVal path As String)


        Dim toDownload As System.IO.FileInfo = New System.IO.FileInfo(HttpContext.Current.Server.MapPath(path))

        HttpContext.Current.Response.Clear()
        HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name)
        HttpContext.Current.Response.AddHeader("Content-Length", toDownload.Length.ToString())
        HttpContext.Current.Response.ContentType = "application/octet-stream"
        HttpContext.Current.Response.WriteFile(path)
        HttpContext.Current.Response.End()

    End Sub
    'Protected Sub btn_uploadpan_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_uploadpan.Click
    '    Try
    '        Dim len As Integer = 0
    '        len = FileUpload_Pan.PostedFile.ContentLength
    '        If len > 0 Then

    '            Dim finfo As New FileInfo(FileUpload_Pan.FileName)
    '            Dim fileExtension As String = finfo.Extension.ToLower()
    '            If fileExtension <> ".jpg" Then
    '                ClientScript.RegisterStartupScript(Page.GetType(), "Alert", "alert('Please Upload JPG formate');", True)
    '            Else

    '                Dim file As String = ""
    '                If FileUpload_Pan.HasFile = True Then
    '                    Dim filepath As String = Server.MapPath("~/AgentPancard/" + "PAN_" + Session("UID"))
    '                    filepath = filepath + ".jpg"
    '                    FileUpload_Pan.SaveAs(filepath.ToString())
    '                    file = Path.GetFileName("PAN_" + Session("UID") + ".jpg")
    '                    ClientScript.RegisterStartupScript(Page.GetType(), "Alert", "alert('Pancard image updated sucessfully');", True)
    '                End If

    '            End If
    '        Else
    '            ClientScript.RegisterStartupScript(Page.GetType(), "Alert", "alert('Please Select Pancard Image');", True)
    '        End If
    '    Catch ex As Exception
    '        clsErrorLog.LogInfo(ex)

    '    End Try
    'End Sub
End Class
