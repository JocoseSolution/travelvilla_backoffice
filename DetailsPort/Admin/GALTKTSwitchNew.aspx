﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageSignIn.master" AutoEventWireup="true" CodeFile="GALTKTSwitchNew.aspx.cs" Inherits="DetailsPort_Admin_GALTKTSwitchNew" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

      <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-1.4.4.min.js") %>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-ui-1.8.8.custom.min.js")%>"></script>
    <style type="text/css">
        .page-wrapperss {
            background-color: #fff;
            margin-left: 15px;
        }
    </style>
    <script type="text/javascript">
        $(function () {
            SearchText();
        });
        function SearchText() {
            $(".autosuggest").autocomplete({
                source: function (request, response) {
                    $.ajax({
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        url: "GALTKTSwitchNew.aspx/GetAutoCompleteData",
                        data: "{'username':'" + $('#<%=txtSearch.ClientID%>').val() + "'}",
                          dataType: "json",
                          success: function (data) {
                              if (data.d.length > 0) {
                                  response($.map(data.d, function (item) {
                                      return {
                                          label: item.split('/')[0],
                                          val: item.split('/')[1]
                                      }
                                  }));
                              }
                              else {
                                  response([{ label: 'No Records Found', val: -1 }]);
                              }
                          },
                          error: function (result) {
                              alert("Error");
                          }
                      });
                  },
                  select: function (event, ui) {
                      if (ui.item.val == -1) {
                          return false;
                      }
                      $('#aircode').val(ui.item.val);
                      $('#airlinename').val(ui.item.label);
                  }
              });
          }
    </script>
   <script type="text/javascript">
       function Validate() {
           var ddlPCC = $('#<%=txtPCC.ClientID%>').val();
           var airline = $('#<%=txtSearch.ClientID%>').val();
           if (airline == "") {
               alert("Please select airline!");
               return false;
           }
           if (ddlPCC == "0") {
               alert("Please select PCC!");
               return false;
           }          
           return true;
    }
   </script>
    <div class="row">
        <div class="col-md-2">
        </div>
        <div class="col-md-10">
            <div class="page-wrapperss">

                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">Flight Setting > Gal Ticketing Switch</h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-4">
                                <label for="exampleInputEmail1">Trip:</label>
                                <div class="form-group">
                                    <asp:DropDownList ID="ddl_gds_tktsearch" runat="server" AutoPostBack="true" CssClass="form-control" OnSelectedIndexChanged="ddl_gds_tktsearch_SelectedIndexChanged">
                                        <asp:ListItem Selected="True" Value="D">Domestic</asp:ListItem>
                                        <asp:ListItem Value="I">International</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-md-4" id="airid" runat="server">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Airline</label>
                                    <asp:TextBox ID="txtSearch" runat="server" CssClass="form-control autosuggest"></asp:TextBox>
                                    <input type="hidden" id="aircode" name="aircode" value="" />
                                    <input type="hidden" id="airlinename" name="airlinename" value="" />
                                </div>
                            </div>
                            <div class="col-md-4">
                                <label for="exampleInputEmail1">PCC :</label>
                                <asp:DropDownList CssClass="form-control" ID="txtPCC" runat="server">
                                </asp:DropDownList>
                               
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Is Parent Page:</label>
                                    <asp:CheckBox ID="CheckBox1" runat="server" CssClass="form-control" Checked="true" />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <asp:Button ID="Submit" runat="server" Text="Submit" CssClass="button buttonBlue" OnClientClick="return Validate()" OnClick="Submit_Click"  />
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <asp:Label ID="lbl_status" runat="server" Font-Bold="True" ForeColor="#009933" Width="550px"></asp:Label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <asp:UpdatePanel runat="server" ID="UP_GDS_TKT_Search">
                                    <ContentTemplate>
                                        <asp:GridView ID="GV_GDS_TKT_Search" runat="server" AllowPaging="True" AutoGenerateColumns="false" CssClass="table" GridLines="None"
                                            DataKeyNames="Counter" OnPageIndexChanging="GV_GDS_TKT_Search_PageIndexChanging" Width="100%" OnRowCancelingEdit="GridView1_RowCancelingEdit" OnRowDeleting="GridView1_RowDeleting"
                                            OnRowEditing="GridView1_RowEditing" OnRowUpdating="GridView1_RowUpdating">
                                            <Columns>
                                                <asp:CommandField ShowEditButton="True" />
                                                <asp:TemplateField HeaderText="counter" Visible="false">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl_counter" runat="server" Text='<%#Eval("counter") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Airline_name">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl_Airline_name" runat="server" Text='<%#Eval("Airline_name") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Airline_Code">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl_Airline_Code" runat="server" Text='<%# Eval("Airline_Code")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Trip">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl_Trip" runat="server" Text='<%#Eval("Trip") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Supplier">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl_Supplier" runat="server" Text='<%#Eval("Supplier") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>                            
                                                <asp:TemplateField HeaderText="Status">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl_IsActive" runat="server" Text='<%#Eval("IsActive") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <EditItemTemplate>
                                            <asp:DropDownList ID="ddl_status" runat="server">
                                                <asp:ListItem Value="True">Active </asp:ListItem>
                                                <asp:ListItem Value="False">Inactive </asp:ListItem>
                                            </asp:DropDownList>
                                        </EditItemTemplate>
                                                </asp:TemplateField>
                                                <asp:CommandField ShowDeleteButton="True" />
                                            </Columns>

                                        </asp:GridView>
                                        </div>
                                          <div class="col-md-6">
                                              <div class="form-group"></div>
                                          </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

     
    <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UP_GDS_TKT_Search">
        <ProgressTemplate>
            <div class="modal">
                <div class="center">
                    <%-- <img src="Images/loader.gif" />--%>
                    Please Wait....
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
</asp:Content>

