﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageSignIn.master" AutoEventWireup="true" CodeFile="AirProviderSwitch.aspx.cs" Inherits="DetailsPort_Admin_AirProviderSwitch" %>

<%@ Register Src="~/UserControl/Settings.ascx" TagPrefix="uc1" TagName="Settings" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <style>
        .mydatagrid
        {
            width: 80%;
            border: solid 2px black;
            min-width: 80%;
        }

        .header
        {
            background-color: #000;
            font-family: Arial;
            color: White;
            height: 25px;
            text-align: center;
            font-size: 12px;
        }



        .rowws
        {
            background-color: #fff;
            font-family: Arial;
            font-size: 14px;
            color: #000;
            min-height: 25px;
            text-align: left;
        }

            .rows:hover
            {
                background-color: #5badff;
                color: #fff;
            }

        .mydatagrid a /** FOR THE PAGING ICONS  **/
        {
            background-color: Transparent;
            padding: 5px 5px 5px 5px;
            color: #fff;
            text-decoration: none;
            font-weight: bold;
        }

            .mydatagrid a:hover /** FOR THE PAGING ICONS  HOVER STYLES**/
            {
                background-color: #49cced;
                color: #fff;
            }

        .mydatagrid span /** FOR THE PAGING ICONS CURRENT PAGE INDICATOR **/
        {
            background-color: #fff;
            color: #000;
            padding: 5px 5px 5px 5px;
        }

        .pager
        {
            background-color: #5badff;
            font-family: Arial;
            color: White;
            height: 30px;
            text-align: left;
        }

        .mydatagrid td
        {
            padding: 5px;
        }

        .mydatagrid th
        {
            padding: 5px;
        }
    </style>

    <link rel="stylesheet" href="../../chosen/chosen.css" />

    <script src="../../chosen/jquery-1.6.1.min.js" type="text/javascript"></script>

    <script src="../../chosen/chosen.jquery.js" type="text/javascript"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            $(".chzn-select").chosen();
            $(".chzn-select-deselect").chosen({ allow_single_deselect: true });
        });
    </script>

    <script type="text/javascript">
        function checkit(evt) {
            evt = (evt) ? evt : window.event
            var charCode = (evt.which) ? evt.which : evt.keyCode


            if (!(charCode == 46 || charCode == 48 || charCode == 49 || charCode == 50 || charCode == 51 || charCode == 52 || charCode == 53 || charCode == 54 || charCode == 55 || charCode == 56 || charCode == 57 || charCode == 8)) {
                //                alert("This field accepts only Float Number.");
                return false;


            }

            status = "";
            return true;

        }


    </script>

      <style type="text/css">
        .page-wrapperss {
            background-color: #fff;
            margin-left: 15px;
        }
    </style>
    <div class="row">
        <div class="col-md-2">
        </div>
        <div class="container-fluid" style="padding-right:35px;">
            <div class="page-wrapperss">

                <div class="panel panel-primary">
                    
                    <div class="panel-body">

                        <div class="row">
                            <div class="col-md-2">                              
                        <%-- <label for="exampleInputPassword1"> Trip Type :</label> --%>                                       
                  <asp:DropDownList ID="ddl_triptype" runat="server" AutoPostBack="True" CssClass="input-text full-width" Width="150px" OnSelectedIndexChanged="ddl_triptype_SelectedIndexChanged">
                      <%--<asp:ListItem Value="I" Selected="True">--TRIP TYPE--</asp:ListItem>--%>
                      <asp:ListItem  value="" disabled selected style="display: none;">SELECT TRIP TYPE</asp:ListItem>
                  <asp:ListItem Value="D">Domestic</asp:ListItem>
                  <asp:ListItem Value="I">International</asp:ListItem>
                     </asp:DropDownList>
                                 </div>
                                </div>
                            <div class="row"><br/></div>
                        <hr />
                        <div class="row">
                                            <div class="col-md-10 container-fluid">
                                                 <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                                 <ContentTemplate>
                                                <asp:Label ID="lbl_status" runat="server" Font-Bold="True" ForeColor="#009933" Width="550px"></asp:Label>
                                                <asp:GridView ID="GridView1" runat="server" AllowPaging="True" AutoGenerateColumns="false"
                                                    DataKeyNames="Airline" OnPageIndexChanging="GridView1_PageIndexChanging"
                                                    OnRowCancelingEdit="GridView1_RowCancelingEdit" HeaderStyle-CssClass="header" CssClass="table" PagerStyle-CssClass="pager"
                                                    OnRowEditing="GridView1_RowEditing" OnRowUpdating="GridView1_RowUpdating" Width="100%" OnRowDataBound="GridView1_RowDataBound">

                                                    <Columns>
                                                        <asp:TemplateField HeaderText="Airline">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbl_Airline" runat="server" Text='<%#Eval("Airline") %>'></asp:Label>
                                                                <asp:Label ID="lbl_counter" runat="server" Visible="false" Text='<%#Eval("Counter") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="RTF">
                                                            <ItemTemplate>
                                                                <asp:CheckBox ID="chk_RTF" runat="server" OnCheckedChanged="chk_RTF_CheckedChanged" AutoPostBack="true" Checked='<%# bool.Parse(Eval("RTF").ToString()) %>' />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Status">
                                                            <ItemTemplate>
                                                                <asp:CheckBox ID="chk_Status" runat="server" AutoPostBack="true" OnCheckedChanged="chk_Status_CheckedChanged" Checked='<%# bool.Parse(Eval("Status").ToString()) %>' />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Provider">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbl_Provider" runat="server" Text='<%# Eval("Provider")%>' Visible="false"></asp:Label>
                                                                <asp:RadioButtonList ID="rbl_Provider" AutoPostBack="true" RepeatDirection="Horizontal" OnSelectedIndexChanged="rbl_Provider_SelectedIndexChanged" runat="server"></asp:RadioButtonList>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Airline Type">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbl_ProviderType" runat="server" Text='<%#Eval("AirlineType") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>
                                                   </ContentTemplate>
                                                    </asp:UpdatePanel>

                                            </div>
                                        </div>

                              
                                <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
                                    <ProgressTemplate>
                                        <div class="modal">
                                            <div class="center">
                                                Please Wait....
                                            </div>
                                        </div>
                                    </ProgressTemplate>
                                </asp:UpdateProgress>

                            </div>
                        </div>

                    </div>
                </div>
            </div>
       
    <%--<style type="text/css">
        body
        {
            margin: 0;
            padding: 0;
            font-family: Arial;
        }
        .modal
        {
            position: fixed;
            z-index: 999;
            height: 100%;
            width: 100%;
            top: 0;
            background-color: Black;
            filter: alpha(opacity=60);
            opacity: 0.6;
            -moz-opacity: 0.8;
        }
        .center
        {
            z-index: 1000;
            margin: 300px auto;
            padding: 10px;
            width: 154px;
            background-color: White;
            border-radius: 10px;
            filter: alpha(opacity=100);
            opacity: 1;
            -moz-opacity: 1;
        }
        .center img
        {
            height: 150px;
            width: 150px;
        }
    </style>--%>

    <%-- <div class="mtop80"></div>
    <div class="large-12 medium-12 small-12">
    <div class="large-3 medium-3 small-12 columns">
       
                <uc1:Settings runat="server" ID="Settings" />
            
    </div>
         <div class="large-8 medium-8 small-12 columns heading end">
                                            <div class="large-12 medium-12 small-12 heading1">Air Povider Switch
                            </div>
                                            <div class="clear1"></div>
        <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
        <ProgressTemplate>
            <div class="modal">
                <div class="center">
                   
                       Please Wait....
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                  <div>
        Trip Type :
        <asp:DropDownList ID="ddl_triptype" runat="server" AutoPostBack="True" Width="150px" OnSelectedIndexChanged="ddl_triptype_SelectedIndexChanged">
            <asp:ListItem Value="D" Selected="True">Domestic</asp:ListItem>
            <asp:ListItem Value="I">International</asp:ListItem>
        </asp:DropDownList>
    </div>
        <table class="auto-style1">
            <tr>
                <td>
                    <asp:Label ID="lbl_status" runat="server" Font-Bold="True" ForeColor="#009933" Width="550px"></asp:Label>
                </td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td colspan="4">
                 <asp:GridView ID="GridView1" runat="server" AllowPaging="True" AutoGenerateColumns="false"
                      DataKeyNames="Airline" OnPageIndexChanging="GridView1_PageIndexChanging"
                      OnRowCancelingEdit="GridView1_RowCancelingEdit" 
                     OnRowEditing="GridView1_RowEditing" OnRowUpdating="GridView1_RowUpdating" Width="100%" OnRowDataBound="GridView1_RowDataBound">--%>
    <%-- <AlternatingRowStyle BackColor="#FFCC66" />--%>
    <%-- <Columns>
                            <asp:TemplateField HeaderText="Airline">
                                <ItemTemplate>
                                    <asp:Label ID="lbl_Airline" runat="server" Text='<%#Eval("Airline") %>'></asp:Label>
                                    <asp:Label ID="lbl_counter" runat="server" Visible="false" Text='<%#Eval("Counter") %>'></asp:Label>
                                </ItemTemplate>
                             </asp:TemplateField>
                          <asp:TemplateField HeaderText="RTF">
                                <ItemTemplate>
                                     <asp:CheckBox ID="chk_RTF" runat="server" OnCheckedChanged="chk_RTF_CheckedChanged" AutoPostBack="true" Checked='<%# bool.Parse(Eval("RTF").ToString()) %>'/> 
                                </ItemTemplate>
                             </asp:TemplateField>
                            <asp:TemplateField HeaderText="Status">
                                <ItemTemplate>
                                    <asp:CheckBox ID="chk_Status" runat="server" AutoPostBack="true" OnCheckedChanged="chk_Status_CheckedChanged" Checked='<%# bool.Parse(Eval("Status").ToString()) %>'/> 
                                </ItemTemplate>
                             </asp:TemplateField>
                         <asp:TemplateField HeaderText="Provider">
                             <ItemTemplate>
                                 <asp:Label ID="lbl_Provider" runat="server" Text='<%# Eval("Provider")%>' Visible="false"></asp:Label>
                                 <asp:RadioButtonList ID="rbl_Provider" AutoPostBack="true" RepeatDirection="Horizontal" OnSelectedIndexChanged="rbl_Provider_SelectedIndexChanged" runat="server"></asp:RadioButtonList>
                             </ItemTemplate>
                         </asp:TemplateField>
                       <asp:TemplateField HeaderText="Airline Type">
                                <ItemTemplate>
                                      <asp:Label ID="lbl_ProviderType" runat="server" Text='<%#Eval("AirlineType") %>'></asp:Label>
                                </ItemTemplate>
                             </asp:TemplateField>
                        </Columns>
                 </asp:GridView>
                </td>
            </tr>
        </table>
            </ContentTemplate>
        </asp:UpdatePanel>
             </div></div>--%>


</asp:Content>

