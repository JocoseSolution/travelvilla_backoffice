﻿<%@ Application Language="C#" %>

<script runat="server">

   protected void Application_Start(object sender, EventArgs e) 
    {
        // Code that runs on application startup
        RouteConfig.RegisterRoutes(System.Web.Routing.RouteTable.Routes);
    }

   protected void Application_End(object sender, EventArgs e) 
    {
        //  Code that runs on application shutdown

    }

   protected void Application_Error(object sender, EventArgs e) 
    { 
        // Code that runs when an unhandled error occurs

    }



   protected void Session_Start(object sender, EventArgs e) 
    {
        // Code that runs when a new session is started

    }

   protected void Session_End(object sender, EventArgs e) 
    {
        // Code that runs when a session ends. 
        // Note: The Session_End event is raised only when the sessionstate mode
        // is set to InProc in the Web.config file. If session mode is set to StateServer 
        // or SQLServer, the event is not raised.

    }
    private static void RegisterRoutes()
    {
        //System.Web.Routing.RouteTable.Routes.Add("Any Description",
        //    new System.Web.Routing.Route("aboutus", new
        //                      System.Web.Routing.PageRouteHandler("~/aboutus.aspx")));
        //System.Web.Routing.RouteTable.Routes.Add("Any Description1",
        //    new System.Web.Routing.Route("contactus", new
        //                      System.Web.Routing.PageRouteHandler("~/contactus.aspx")));

    }
       
</script>
