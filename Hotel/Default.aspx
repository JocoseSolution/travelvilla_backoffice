﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Default.aspx.vb" Inherits="Hotel_Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <div class='w100 bld'>
                <div class='w25 lft'>Country Name</div>
                <div class='w25 lft'>Currency Name</div>
                <div class='w25 lft'>Update Date</div>
                <div class='w25 lft'>
                    <div class='w70 lft'>Exchange Rate</div>
                    <div class='w30 lft'>Edit Delete</div>
                </div>
            </div>
            <div class='clear'></div>
            <div class='w100'>
                <div class='w25 lft'>Austria (AT)</div>
                <div class='w25 lft'>Euro (EUR)</div>
                <div class='w25 lft'>5/4/2016 11:23:19 AM</div>
                <div class='w25 lft'>
                    <div class='w70 lft'><span class='display-mode' id='cur116'>70</span>
                        <input type='text' id='txt116' value="70" class='edit-mode' style='width: 83px;' /></div>
                    <a class='edit-user display-mode'>
                        <img src='/Images/edit-icon.png' title='Edit' /></a> 
                       <a class='save-user edit-mode' id="116">
                            <img src='/Images/ok.png' title='Save' /></a> 
                    <a class='cancel-user edit-mode'>
                                <img src='/Images/cancel-icon.png' title='Cancel' /></a> 
                    <a class='topbutton Delete' title='Delete' onclick='Deletediscdetails(116)'>
                                    <img src='/Images/delete-icon.png' title='Delete' />
                                </a></div>
            </div>
        </div>
    </form>
</body>
</html>
