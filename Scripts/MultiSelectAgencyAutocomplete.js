﻿
$(document).ready(function () {
    $("#<%=txtSearch.ClientID %>").autocomplete({
        source: function (request, response) {
            $.ajax({
                // url: ~/AgencySearch.asmx/GetCustomers
                //data: "{ 'prefix': '" + request.term + "'}",
                url: UrlBase + "AgencySearch.asmx/FetchAgencyList",//'<%=ResolveUrl("~/AgencySearch.asmx/FetchAgencyList") %>',
                data: "{ 'city': '" + request.term + "', maxResults: 10 }",
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    response($.map(data.d, function (item) {
                        return {

                            label: item.Agency_Name + "(" + item.User_Id + ")",
                            val: item.User_Id
                            //label: item.split('-')[0],
                            //val: item.split('-')[1]
                        }
                    }))
                },
                error: function (response) {
                    //alert(response.responseText);
                },
                failure: function (response) {
                    // alert(response.responseText);
                }
            });
        },
        select: function (e, i) {
            var text = this.value.split(/,\s*/);
            //text.pop();
            //text.push(i.item.value);
            //text.push("");
            //this.value = text.join(", ");
            this.value = "";
            var value = $("[id*=hfCustomerId]").val().split(/,\s*/);

            var maincontent = "";
            value.pop();
            value.push(i.item.val);
            value.push("");
            $("#[id*=hfCustomerId]")[0].value = value.join(",");
            var UserCount = $("#[id*=hfCustomerId]")[0].value.split(",").length;
            if (UserCount > 1) {
                var max = UserCount - 2
                for (var n = 0; n <= UserCount - 2; n++) {
                    if (max == 0 || n == max) {
                        maincontent = maincontent + "<span onclick=\"SetAndRemove('" + $("#[id*=hfCustomerId]")[0].value.split(",")[n] + "')\">" + $("#[id*=hfCustomerId]")[0].value.split(",")[n] + "&nbsp<img src='../../Images/close.gif' />&nbsp</span>";
                    }
                    else {
                        maincontent = maincontent + "<span onclick=\"SetAndRemove('" + $("#[id*=hfCustomerId]")[0].value.split(",")[n] + "')\">" + $("#[id*=hfCustomerId]")[0].value.split(",")[n] + "&nbsp<img src='../../Images/close.gif' />&nbsp,</span>";
                    }


                }
                document.getElementById("content").innerHTML = maincontent;
            }
            return false;
        },
        minLength: 1
    });

});


 function checkShortcut() {
            //if (event.keyCode == 8 || event.keyCode == 13 || event.keyCode == 46) {
            //    return false;
            //}
 }

function ClearRec() {
    $("#ctl00_ContentPlaceHolder1_txtSearch").val("");
    //$("#ctl00_ContentPlaceHolder1_hfCustomerId").val("");
}

function SetAndRemove(UserId) {
    //$("#ctl00_ContentPlaceHolder1_txtSearch").val("");
    //$("#ctl00_ContentPlaceHolder1_hfCustomerId").val("");

    var maincontent = "";
    var AgentId = "";
    var UserCount = $("#[id*=hfCustomerId]")[0].value.split(",").length;
    if (UserCount > 1) {
        var max = UserCount - 2
        for (var n = 0; n <= UserCount - 2; n++) {
            if ($("#[id*=hfCustomerId]")[0].value.split(",")[n] != UserId)
            {
                if (max == 0 || n == max) {
                    maincontent = maincontent + "<span onclick=\"SetAndRemove('" + $("#[id*=hfCustomerId]")[0].value.split(",")[n] + "')\">" + $("#[id*=hfCustomerId]")[0].value.split(",")[n] + "&nbsp<img src='../../Images/close.gif' />&nbsp</span>";
                    AgentId =AgentId+ $("#[id*=hfCustomerId]")[0].value.split(",")[n] + ',';
                }
                else {
                    maincontent = maincontent + "<span onclick=\"SetAndRemove('" + $("#[id*=hfCustomerId]")[0].value.split(",")[n] + "')\">" + $("#[id*=hfCustomerId]")[0].value.split(",")[n] + "&nbsp<img src='../../Images/close.gif' />&nbsp,</span>";
                    AgentId = AgentId + $("#[id*=hfCustomerId]")[0].value.split(",")[n] + ',';
                }
            }
        }
        document.getElementById("content").innerHTML = maincontent;
        $("#ctl00_ContentPlaceHolder1_hfCustomerId").val(AgentId);
    }
}

