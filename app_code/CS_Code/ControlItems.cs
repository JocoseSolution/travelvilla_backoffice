﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using System.Net.Mail;
using System.Globalization;
public class ControlItems
{
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString);
    public ControlItems()
    { }
    public string InsertControlDetails(string ControlName, int Width, int Height, int DisplayTime, string CreatedBy, string Cmd_Type)
    {
        string ResultVal = "";
        try
        {
            SqlCommand cmd = new SqlCommand("USP_INSERTCONTROL", con);
            cmd.CommandType = CommandType.StoredProcedure;
            //cmd.Parameters.AddWithValue("@ControlID", ControlID);
            cmd.Parameters.AddWithValue("@ControlName", ControlName);
            cmd.Parameters.AddWithValue("@Width", Width);
            cmd.Parameters.AddWithValue("@Height", Height);
            cmd.Parameters.AddWithValue("@DisplayTime", DisplayTime);
            cmd.Parameters.AddWithValue("@CreatedBy", CreatedBy);
            cmd.Parameters.AddWithValue("@Cmd_Type", Cmd_Type);
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
            else
            {
                con.Open();
            }
            ResultVal = Convert.ToString(cmd.ExecuteScalar());
            con.Close();
        }
        catch (Exception ex)
        {

        }
        return ResultVal;
    }
    public DataTable SelectDDL(string Cmd_Type)
    {
        DataTable DTDDL = new DataTable();
        try
        {
            SqlCommand cmd = new SqlCommand("USP_INSERTCONTROL", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@ControlName", "");
            cmd.Parameters.AddWithValue("@Width", 0);
            cmd.Parameters.AddWithValue("@Height", 0);
            cmd.Parameters.AddWithValue("@DisplayTime", 0);
            cmd.Parameters.AddWithValue("@CreatedBy", "");
            cmd.Parameters.AddWithValue("@Cmd_Type", Cmd_Type);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
            else
            {
                con.Open();
            }
            da.Fill(DTDDL);
            con.Close();
        }
        catch (Exception ex)
        {

        }
        return DTDDL;
    }
    public string InsertControlWithItemID(int ControlID, string controlName, string title, string subtitle, decimal price, string ImageURL, string LinkedURL, string CMD_TYPE, string CreatedBy)
    {
        string ResultVal = "";
        try
        {
            SqlCommand cmd = new SqlCommand("USP_INSERTCONTROLITEMS", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@ControlID", ControlID);
            cmd.Parameters.AddWithValue("@controlName", controlName);
            cmd.Parameters.AddWithValue("@title", title);
            cmd.Parameters.AddWithValue("@subtitle", subtitle);
            cmd.Parameters.AddWithValue("@price", price);
            cmd.Parameters.AddWithValue("@ImageURL", ImageURL);
            cmd.Parameters.AddWithValue("@LinkedURL", LinkedURL);
            cmd.Parameters.AddWithValue("@CreatedBy", CreatedBy);
            cmd.Parameters.AddWithValue("@CMD_TYPE", CMD_TYPE);
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
            else
            {
                con.Open();
            }
            ResultVal = Convert.ToString(cmd.ExecuteScalar());
            con.Close();
        }
        catch (Exception ex)
        {

        }
        return ResultVal;
    }
    public DataTable BindGrid(string Cmd_Type)
    {
        DataTable DTDDL = new DataTable();
        try
        {
            SqlCommand cmd = new SqlCommand("USP_INSERTCONTROLITEMS", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@ControlID", 0);
            cmd.Parameters.AddWithValue("@controlName", "");
            cmd.Parameters.AddWithValue("@title", "");
            cmd.Parameters.AddWithValue("@subtitle", "");
            cmd.Parameters.AddWithValue("@price", 0.00);
            cmd.Parameters.AddWithValue("@ImageURL", "");
            cmd.Parameters.AddWithValue("@LinkedURL", "");
            cmd.Parameters.AddWithValue("@CreatedBy", "");
            cmd.Parameters.AddWithValue("@CMD_TYPE", Cmd_Type);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
            else
            {
                con.Open();
            }
            da.Fill(DTDDL);
            con.Close();
        }
        catch (Exception ex)
        {

        }
        return DTDDL;
    }
    public string UpdateControlWithItemID(int ItemID, string title, string subtitle, decimal price, string ImageURL, string LinkedURL, string CMD_TYPE, string CreatedBy)
    {
        string ResultVal = "";
        try
        {
            SqlCommand cmd = new SqlCommand("USP_INSERTCONTROLITEMS", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@ControlID", ItemID);
            cmd.Parameters.AddWithValue("@controlName", "");
            cmd.Parameters.AddWithValue("@title", title);
            cmd.Parameters.AddWithValue("@subtitle", subtitle);
            cmd.Parameters.AddWithValue("@price", price);
            cmd.Parameters.AddWithValue("@ImageURL", ImageURL);
            cmd.Parameters.AddWithValue("@LinkedURL", LinkedURL);
            cmd.Parameters.AddWithValue("@CreatedBy", CreatedBy);
            cmd.Parameters.AddWithValue("@CMD_TYPE", CMD_TYPE);
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
            else
            {
                con.Open();
            }
            ResultVal = Convert.ToString(cmd.ExecuteScalar());
            con.Close();
        }
        catch (Exception ex)
        {
        }
        return ResultVal;
    }
    public string DeleteControlWithItemID(int ItemID, string CMD_TYPE)
    {
        string ResultVal = "";
        try
        {
            SqlCommand cmd = new SqlCommand("USP_INSERTCONTROLITEMS", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@ControlID", ItemID);
            cmd.Parameters.AddWithValue("@controlName", "");
            cmd.Parameters.AddWithValue("@title", "");
            cmd.Parameters.AddWithValue("@subtitle", "");
            cmd.Parameters.AddWithValue("@price", 0.00);
            cmd.Parameters.AddWithValue("@ImageURL", "");
            cmd.Parameters.AddWithValue("@LinkedURL", "");
            cmd.Parameters.AddWithValue("@CreatedBy", "");
            cmd.Parameters.AddWithValue("@CMD_TYPE", CMD_TYPE);
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
            else
            {
                con.Open();
            }
            ResultVal = Convert.ToString(cmd.ExecuteScalar());
            con.Close();
        }
        catch (Exception ex)
        {
        }
        return ResultVal;
    }
}